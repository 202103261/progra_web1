class Character {
  constructor(x, y, width, height, speed) {
    this.x = x;
    this.y = y;
    this.width = width;
    this.height = height;
    this.speed = speed;
    this.image = new Image();
    this.iz = false;
  }

  moveLeft() {
    if (this.x > 0) {
      this.x -= this.speed;
    }
    if (!this.iz) {
      this.iz = true;
    }
  }

  moveRight(max) {
    if (this.x + this.width + this.speed < max) {
      this.x += this.speed;
    }
    if (this.iz) {
      this.iz = false;
    }
  }
}
