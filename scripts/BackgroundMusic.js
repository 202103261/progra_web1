class BackgroundMusic {
  /*    The music content was downloades from:
        https://www.chosic.com/download-audio/45393/    */
  constructor(pathFile) {
    this.audio = new Audio(pathFile);
    this.audio.loop = true;
    this.isPlaying = false;
    this.audio.load();
  }

  play() {
    if (!this.isPlaying) {
      this.audio.play();
      this.isPlaying = true;
    }
  }

  pause() {
    if (this.isPlaying) {
      this.audio.pause();
      this.isPlaying = false;
    }
  }

  stop() {
    if (this.isPlaying) {
      this.audio.pause();
      this.audio.currentTime = 0;
      this.isPlaying = false;
    }
  }
}
