class Hero extends Character {
  constructor(x, y, width, height, speed, ground) {
    super(x, y, width, height, speed);
    this.canJump = false;
    this.downAfterJump = false;
    this.ground = ground;
    this.jumpDistance = y;
    this.image.src = "./assets/hero.png";
  }

  jump() {
    if (!this.canJump) this.canJump = true;
  }

  jumping() {
    if (this.canJump) {
      this.y -= 2;
      this.jumpDistance += 2;

      if (this.jumpDistance > (this.height * 85) / 100) {
        this.canJump = false;
        this.downAfterJump = true;
      }
    } else if (this.downAfterJump) {
      this.y += 2;
      this.jumpDistance -= 2;

      if (this.jumpDistance < this.ground) this.downAfterJump = false;
    }
  }
}
