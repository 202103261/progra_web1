class Mosquito extends Character {
  constructor(x, y, width, height, speed, dist) {
    super(x, y, width, height, speed);
    this.a = 250;
    this.b = 80;
    this.dist = dist;
    this.angle = 0;
    this.finalizar = false;
    this.image.src = "./assets/mosquito.png";
  }
  move() {
      this.x += this.a * Math.cos(this.angle);
      this.y += this.b * Math.sin(this.angle);
      this.angle += 8;
  }
  update(hero, canvas) {
    let hx = hero.x;
    let hy = hero.y;
    let dx = (hx-this.x); 
    let dy = (hy-this.y);
    let aux = Math.sqrt(dx*dx + dy*dy);
    if (aux<this.dist) {
        if (Math.abs(dx)>hero.width/5 || Math.abs(dy)>hero.height/5) {
            if (Math.abs(dx)>Math.abs(dy)) {
                if (dx>=0) 
                    this.moveX(true, canvas.width);
                else 
                    this.moveX(false, canvas.width);
            } else if (dy<0) 
                this.moveY(false, canvas.height);
              else 
                this.moveY(true, canvas.height);
        } else {
            this.finalizar = true;
        }
    } else {
      this.move();
    }
  }
  moveX(a, max) {
    if (a) {
      super.moveRight(max);
    } else if (this.x > 0) super.moveLeft();
  }
  moveY(a, max) {
    if (a) {
      if (this.y + this.height + this.speed < max);
      this.y += this.speed;
    } else if (this.y > 0) this.y -= this.speed;
  }
}
