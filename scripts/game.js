class Game {
  constructor(canvas) {
    this.canvas = canvas;
    this.width = canvas.width;
    this.height = canvas.height;
    this.cxt = canvas.getContext("2d");
    this.dificultad = 10;
    this.keyPressed = {};

    this.app = false;

    this.hero = new Hero(30, 0, 0, 0, 8, 0);
    this.mosquitos = [
      new Mosquito(0, 0, 0, 0, 3, 400),
      new Mosquito(0, 0, 0, 0, 3, 400),
    ];
    this.setResize();

    this.positionX = 0;
    this.right = true;
    this.fondo = new Image();
    this.fondo.src = "./assets/fondo.jpg";
    this.fondo.onload = this.loadBG(this.fondo, this.positionX);

    this.fondo2 = new Image();
    this.fondo2.src = "./assets/fondo.jpg";
    this.fondo2.onload = this.loadBG(this.fondo2, this.positionX);

    this.BackgroundMusic = new BackgroundMusic(
      "/assets/Dance-in-the-Desert.mp3"
    );
  }

  init() {
    this.app = true;
    this.inicio = new Date();
    this.loadBackgroundMusic();
    document.addEventListener("keydown", (e) => {
      this.keyPressed[e.code] = true;
    });
    document.addEventListener("keyup", (e) => {
      this.keyPressed[e.code] = false;
    });
    const jumpButton = document.getElementById("btn-up");
    const moveLeftButton = document.getElementById("btn-left");
    const moveRightButton = document.getElementById("btn-right");

    this.jumpPressed = false;
    this.leftPressed = false;
    this.rightPressed = false;

    jumpButton.addEventListener("touchstart", () => {
      this.jumpPressed = true;
    });

    moveLeftButton.addEventListener("touchstart", () => {
      this.leftPressed = true;
    });

    moveRightButton.addEventListener("touchstart", () => {
      this.rightPressed = true;
    });

    document.addEventListener("touchend", (e) => {
      if (e.target == jumpButton) {
        this.jumpPressed = false;
      } else if (e.target == moveLeftButton) {
        this.leftPressed = false;
      } else if (e.target == moveRightButton) {
        this.rightPressed = false;
      }
    });

    this.intervalMov = setInterval(() => {
      if (this.keyPressed["ArrowRight"] || this.rightPressed) {
        this.hero.moveRight(this.canvas.width);
        this.updatePositionXRight();
      }
      if (this.keyPressed["ArrowLeft"] || this.leftPressed) {
        this.hero.moveLeft();
        this.updatePositionXLeft();
      }
      if (
        (this.keyPressed["ArrowUp"] || this.jumpPressed) &&
        !this.hero.canJump &&
        !this.hero.downAfterJump
      )
        this.hero.jump();
    }, this.dificultad);
    this.intervalGame = setInterval(this.play.bind(this), 10);
  }

  loadBackgroundMusic() {
    this.BackgroundMusic.play();
  }

  loadBG(fondo, dx) {
    this.cxt.drawImage(fondo, dx, 0, this.canvas.width, this.canvas.height);
  }

  updatePositionXRight() {
    if (this.positionX <= -this.canvas.width) {
      this.positionX = 0;
    } else {
      this.positionX -= this.dificultad / 4.0;
    }
  }

  updatePositionXLeft() {
    if (this.positionX >= 0) {
      this.positionX = -this.canvas.width;
    } else {
      this.positionX += this.dificultad / 4.0;
    }
  }
  play() {
    this.cxt.clearRect(0, 0, this.canvas.width, this.canvas.height);

    this.loadBG(this.fondo, Math.round(this.positionX));
    this.loadBG(this.fondo2, this.canvas.width + Math.round(this.positionX));

    this.hero.jumping();
    this.drawCharacter(this.hero);
    let xp = false;
    this.mosquitos.forEach((element, index, array) => {
      element.update(this.hero, this.canvas);
      xp |= element.finalizar === true;
      this.drawCharacter(element);
    });
    if (xp) this.end();
  }

  drawCharacter(character) {
    this.cxt.save();
    if (character.iz) {
      let a = character.x;
      this.cxt.translate(a, character.y);
      this.cxt.scale(-1, 1);
      this.cxt.drawImage(
        character.image,
        0,
        0,
        -character.width,
        character.height
      );
    } else
      this.cxt.drawImage(
        character.image,
        character.x,
        character.y,
        character.width,
        character.height
      );
    this.cxt.restore();
  }

  setResize(w, h) {
    this.ground = Math.round((this.canvas.height * 5) / 100);
    let temp = (this.canvas.height * 35) / 100;
    this.hero.width = Math.round(temp);
    this.hero.height = Math.round(temp);
    this.hero.y = this.canvas.height - temp - this.ground;

    this.xPivote = (this.canvas.width * 65) / 100;
    this.yPivote = (this.canvas.height * 25) / 100;
    this.mosquitoSize = (this.canvas.height * 15) / 100;

    let a = Math.max(this.canvas.width, this.canvas.height);
    let b = Math.min(this.canvas.width, this.canvas.height);
    this.hero.speed = (a/b)*3; 

    this.mosquitos.forEach((element, index, array) => {
        element.x = this.xPivote;
        element.y = this.yPivote;
        element.width = this.mosquitoSize;
        element.height = this.mosquitoSize;
        element.speed = this.hero.speed / 5;
    });
}

  end() {
    this.app = false;
    this.BackgroundMusic.stop();
    this.gameOver = new Date();
    let aux = this.gameOver.getTime() - this.inicio.getTime();
    aux = Math.round(aux / 1000);
    clearInterval(this.intervalGame);
    clearInterval(this.intervalMov);
    window.alert(`Juego terminado, tiempo alcanzado: ${aux}`);
    this.BackgroundMusic.stop();
  }
}
