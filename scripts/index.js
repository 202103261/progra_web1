const canvas = document.getElementById("juego");
canvas.width = window.innerWidth;
canvas.height = window.innerHeight;

let game = new Game(canvas);
game.init();

addEventListener("resize", () => {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
  game.setResize();
});
